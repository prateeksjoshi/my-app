import React from 'react';
import {Redirect,Link} from 'react-router-dom';

import api from '../restApi';

class CustomerList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      customers: [],
      fireRedirect:false,
      customerIndex:0
    };
  }

  componentDidMount(){
    this.customerList();
  }

  async customerList(){
    const response = await fetch(api);
    const responseData = await response.json();
    this.setState({customers:responseData});
  }

  editCustomer(id){
    this.setState({
      fireRedirect:true,
      customerIndex:id
    })
  }

  async deleteCustomer(id,index){
    await fetch(api+"/"+id,{
      method:"delete",
      headers:{
        "Content-type":"application/json"
      }
    });
    delete this.state.customers[index];
    let customers = this.state.customers;
    this.setState({customers:customers});
  }

  render(){
    const redirect = this.state.fireRedirect;
    const custIndex = this.state.customerIndex;
    const customers = this.state.customers.map((customer,index)=>(
      <li key={customer.id}>
        <label>Name</label> : {customer.name}<br/>
        <label>Phone</label> : {customer.phone}<br/>
        <label>City</label> : {customer.city}<br/>
        <button type="button" onClick={()=>this.editCustomer(customer.id)}>Edit</button>
        <button type="button" onClick={()=>this.deleteCustomer(customer.id,index)}>Delete</button>
      </li>
    ));
    return(
      <div>
        <Link to="/add-customer"><button type="button">Add Customer</button></Link>
        <h4>Customer List</h4>
        <ul className="customer-list" id="customerList">
          {customers}
        </ul>
        {redirect && (
          <Redirect to={`/edit-customer/${custIndex}`} />
        )}
      </div>
    );
  }
}

export default CustomerList;
