import React from 'react';
import {Redirect} from 'react-router-dom';

import api from '../restApi';

import Input from '../component/input';

class CustomerAddEdit extends React.Component {
  constructor(props) {
    super(props);
    console.log('check customer id',this.props);
    this.addCustomer = this.addCustomer.bind(this);
    this.handleCustomerName = this.handleCustomerName.bind(this);
    this.handleCustomerCity = this.handleCustomerCity.bind(this);
    this.handleCustomerPhoneNumber = this.handleCustomerPhoneNumber.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

    this.state={
      customer:{
        name:'',
        phone:0,
        city:''
      },
      isEdit:false,
      fireRedirect:false
    }
  }

  componentDidMount(){
    if(this.props.match.params.id){
      this.getCustomer(this.props.match.params.id);
      this.setState({isEdit:true});
    }
  }

  async addCustomer(){
    let customer = {
      name:this.state.customer.name,
      phone:this.state.customer.phone,
      city:this.state.customer.city
    }
    const response = await fetch(api,{
      method:"post",
      headers:{
        "Content-type":"application/json"
      },
      body:JSON.stringify(customer)
    });
    if(response.ok){
      this.setState({
        fireRedirect:true
      })
    }
  }

  async editCustomer(id){
    let customer = {
      name:this.state.customer.name,
      phone:this.state.customer.phone,
      city:this.state.customer.city
    }
    const response = await fetch(api+"/"+id,{
      method:"put",
      headers:{
        "Content-type":"application/json"
      },
      body:JSON.stringify(customer)
    });
    if(response.ok){
      this.setState({
        fireRedirect:true
      })
    }
  }

  async getCustomer(id){
    const response = await fetch(api+"/"+id);
    if(response.ok){
      const responseData = await response.json();
      this.setState({
        customer:responseData
      })
    }
  }

  handleCustomerName(e){
    let customer = this.state.customer;
    customer.name = e.target.value;
    this.setState({
       customer:customer
    });
  }

  handleCustomerPhoneNumber(e){
    let customer = this.state.customer;
    customer.phone = e.target.value;
    this.setState({
       customer:customer
    });
  }

  handleCustomerCity(e){
    let customer = this.state.customer;
    customer.city = e.target.value;
    this.setState({
       customer:customer
    });
  }

  handleSubmit(e){
    e.preventDefault();
    if(this.state.isEdit){
      this.editCustomer(this.state.customer.id);
    }else{
      this.addCustomer();
    }
  }
  render(){

    return(
      <section>
      <form onSubmit={this.handleSubmit}>
        <div>
          <label>Customer Name</label> :
          <Input propType="text" propValue={this.state.customer.name} propOnChange={this.handleCustomerName} propPlaceholder="Customer Name"/>
        </div>
        <div>
          <label>Phone Number</label> :
          <Input propType="text" propValue={this.state.customer.phone} propOnChange={this.handleCustomerPhoneNumber} propPlaceholder="Phone Number"/>
        </div>
        <div>
          <label>City</label> :
          <Input propType="text" propValue={this.state.customer.city} propOnChange={this.handleCustomerCity} propPlaceholder="City"/>
        </div>
        <button type="submit">{this.state.isEdit? "Update" : "Save"}</button>
      </form>
      {this.state.fireRedirect && (
        <Redirect to="/" />
      )}
      </section>
    );
  }
}

export default CustomerAddEdit;
