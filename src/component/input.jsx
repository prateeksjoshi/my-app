import React from 'react';

const Input = (props)=> {
    return(
      <input type={props.propType}
            value={props.propValue}
            onChange={props.propOnChange}
            placeholder={props.propPlaceholder} />
    );
}

export default Input;
